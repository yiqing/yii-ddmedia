<style type="text/css">
<!--
table.media-dir-nav { width:0; }
table.media-dir-nav td { padding: 1px; } 
table.media-items td.folder img { width: 40px; height: 40px; } 
.datahighlight { background-color: #ffdc87 !important; }
.datahighlight2 { background-color: #C3D9FF !important; }
div.ui-dialog form div.simple { margin: 5px 3px 5px 3px; }
// -->
</style>
<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
$this->menu = array(
    //array('label'=>'Rename','url'=>'javascript:void(0)', 'linkOptions'=>array('onclick'=>"showDialog('rename');")),
    //array('label'=>'Move','url'=>'javascript:void(0)', 'linkOptions'=>array('onclick'=>"showDialog('move');")),
    //array('label'=>'Delete','url'=>'javascript:void(0)', 'linkOptions'=>array('onclick'=>"showDialog('delete');")),
    array('label'=>Yii::t('main','Create New Dir'),'url'=>'javascript:void(0)', 'linkOptions'=>array('onclick'=>"doShowDialog=true;showDialog('newdir');")),
    array('label'=>Yii::t('main','Upload File'),'url'=>'javascript:void(0)', 'linkOptions'=>array('onclick'=>"doShowDialog=true;showDialog('upload');")),
);
?>

<h1><?php echo CHtml::encode(Yii::t('main', 'Media Browser')); ?></h1>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
?>

<h2><?php echo str_replace(array($basePath.'/','/'),array('',' / '),$currentPath); ?></h2>

<?php
// echo "<li>basePath: $basePath";
// echo "<li>currentPath: $currentPath";
$relativePath = str_replace($basePath.'/','',$currentPath);
if($relativePath==$currentPath)
    $relativePath='';
// echo "<li>relativePath: $relativePath";
$subDirs = explode('/',$relativePath);
if($subDirs==array(''))
    $subDirs = array();
// echo "<h3>subDirs</h3>";
// var_dump($subDirs);
$dirsBreadcrumbs = $dirsBreadcrumbs2 = $links = array();
foreach($subDirs as $n=>$subDir) {
    $dirsBreadcrumbs[$n] = $subDir;
    if(isset($dirsBreadcrumbs[$n-1]))
        $dirsBreadcrumbs[$n] = $dirsBreadcrumbs[$n-1].'/'.$dirsBreadcrumbs[$n];
}
// echo "<h3>dirsBreadcrumbs</h3>";
// var_dump($dirsBreadcrumbs);
foreach($dirsBreadcrumbs as $n=>$subDir) {
    $dirsBreadcrumbs2[basename($subDir)] = $subDir;
}
// echo "<h3>dirsBreadcrumbs2</h3>";
// var_dump($dirsBreadcrumbs2);
$links[] = array(
    'link'=>CHtml::link(
        Yii::t('main','Base Path'),
        array('index')),
    'subDirs'=>DDMediaDirectory::getSubDirs($basePath)
);
foreach($dirsBreadcrumbs2 as $title=>$subDir) {
    $links[] = array(
        'path'=>urlencode($subDir),
        'link'=>CHtml::link($title,array('index','p'=>urlencode($subDir))),
        'subDir'=>$title,
        'subDirs'=>DDMediaDirectory::getSubDirs($basePath.'/'.$subDir)
    );
}
// echo "<h3>links</h3>";
// var_dump($links);

echo '<form id="dirForm" method="get" action="'.$this->createUrl('index').'">';
echo '<input type="hidden" id="r" name="r" value="media" />';
echo '<input type="hidden" id="p" name="p" size="20" />';
echo '<table class="media-dir-nav"><tr valign="top">';
foreach($links as $i=>$link) {
    echo '<td nowrap="nowrap" style="vertical-align:top">';
    echo $link['link'].'<br />';
    if(count($link['subDirs'])>0) {
        echo '<select size="3" onchange="if(jQuery(this).val()!=\'\') {jQuery(\'#p\').val(this.value);jQuery(\'#dirForm\').submit();}">';
        // echo '<option value="">'.(isset($link['subDir']) ? $link['subDir'] : '(Base Path)').'</option>';
        // echo '<option value="">-</option>';
        foreach($link['subDirs'] as $subDir) {
            $value='';
            if(isset($link['path']))
                $value = $link['path'].'/';
            echo '<option value="'.$value.$subDir.'">'.$subDir.'</option>';
        }
        echo '</select>';
    }
    echo "</td>";
    if(isset($links[$i+1]))
        echo '<td style="font-weight:bold;padding: 0 3px 0 3px;vertical-align:top">/</td>';
}
echo "</tr></table></form>";
?>

<?php if(trim($msg)!=='') : ?>
<p>
<?php echo $msg; ?>
</p>
<?php endif; ?>

<?php 
// Yii::import('DDVarDumper');
// Yii::app()->clientScript->registerCoreScript('jquery');
// DDVarDumper::dumpAsList($files); 
?>

<table class="media-items">
    <thead>
        <tr>
            <th><?php echo CHtml::encode(Yii::t('main', 'Icon')); ?></th>
            <th><?php echo CHtml::encode(Yii::t('main', 'Name')); ?></th>
            <th><?php echo CHtml::encode(Yii::t('main', 'Date')); ?></th>
            <th><?php echo CHtml::encode(Yii::t('main', 'Type')); ?></th>
            <th><?php echo CHtml::encode(Yii::t('main', 'Size')); ?></th>
            <th><?php echo CHtml::encode(Yii::t('main', 'Action')); ?></th>
        </tr>
    </thead>
    <tbody>
        <!-- {{{ Dirs -->
        <?php foreach($files['dirs'] as $dirPath=>$stats) : ?>
        <?php /* $onclick=""; if(!in_array($stats['name'],array('.', '..'))) */ $onclick=' onclick="selectMedia(\'directory\',\''.$dirPath.'\',\''.$stats['name'].'\');"'; ?>
        <tr class="dirsFilesRows"<?php echo $onclick; ?>>
            <td class="folder"><?php echo CHtml::image($this->module->assetsUrl.'/filetypeicons/folder.png',$stats['name']); ?></td>
            <td><?php echo CHtml::link($stats['name'],array('index','p'=>urlencode($path.'/'.$stats['name']))); ?></td>
            <td>&ndash;</td>
            <td><?php echo CHtml::encode(Yii::t('main','Directory')); ?></td>
            <td style="white-space:nowrap;font-size:smaller;text-align:right"><?php echo $stats['size']; ?></td>
            <td style="white-space:nowrap;font-size:smaller">
                <?php if(!in_array($stats['name'],array('..'))) : ?>
                <a href="javascript:void(0)" onclick="selectMedia('directory','<?php echo $dirPath; ?>','<?php echo $stats['name']; ?>');showDialog('rename');" title="<?php echo Yii::t('main', 'Rename directory {dir}', array('{dir}'=>$stats['name'])); ?>"><?php echo CHtml::encode(Yii::t('main', 'IconRename')); ?></a>&nbsp;
                <a href="javascript:void(0)" onclick="selectMedia('directory','<?php echo $dirPath; ?>','<?php echo $stats['name']; ?>');showDialog('copy');" title="<?php echo Yii::t('main', 'Copy directory {dir}', array('{dir}'=>$stats['name'])); ?>"><?php echo CHtml::encode(Yii::t('main', 'IconCopy')); ?></a>&nbsp;
                <a href="javascript:void(0)" onclick="selectMedia('directory','<?php echo $dirPath; ?>','<?php echo $stats['name']; ?>');showDialog('move');" title="<?php echo Yii::t('main', 'Move directory {dir}', array('{dir}'=>$stats['name'])); ?>"><?php echo CHtml::encode(Yii::t('main', 'IconMove')); ?></a>&nbsp;
                <a href="javascript:void(0)" onclick="selectMedia('directory','<?php echo $dirPath; ?>','<?php echo $stats['name']; ?>');showDialog('delete');" title="<?php echo Yii::t('main', 'Delete directory {dir}', array('{dir}'=>$stats['name'])); ?>"><?php echo CHtml::encode(Yii::t('main', 'IconDelete')); ?></a>
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
        <!-- }}} -->
        <!-- {{{ Files -->
        <?php foreach($files['files'] as $filePath=>$stats) : ?>
        <?php $onclick=""; if(!in_array($stats['name'],array('.', '..'))) $onclick=' onclick="selectMedia(\'file\',\''.$filePath.'\',\''.$stats['name'].'\');"'; ?>
        <tr class="dirsFilesRows"<?php echo $onclick; ?>>
            <td>
<?php
if(preg_match("/image\/(.*)/",$stats['mimeType'],$matches))
    echo CHtml::image($this->createUrl('thumbnail',array('path'=>urlencode($stats['path']),'x'=>75)));
else
    echo CHtml::image($this->createUrl('thumbnail',array('path'=>urlencode($stats['path']),'x'=>0)), 'File', array( 'style'=>'width: 40px; height: 40px;' ) );
?>
            </td>
            <td>
<?php
if(preg_match("/image\/(.*)/",$stats['mimeType'],$matches))
    echo CHtml::link($stats['name'],'javascript:void(0)',array('onclick'=>"jQuery('#imagePreview').dialog('open');jQuery('#imagePlaceholder').html('".CHtml::image($this->createUrl('thumbnail',array('path'=>urlencode($stats['path']),'x'=>480)))."');"));
elseif(preg_match("/text\/plain/",$stats['mimeType'],$matches)) {
    echo CHtml::link($stats['name'],'javascript:void(0)',array('onclick'=>'
$.ajax({
    url: "'.$this->createUrl('preview',array('path'=>urlencode($stats['path']))).'",
}).done(function(data) { 
    $("#imagePreview").dialog("open");
    $("#imagePreview").html(data);
});')); 
}
else
    echo CHtml::link($stats['name'],array('download','path'=>urlencode($stats['path'])),array('target'=>'_blank')); 
?></td>
            <td><?php echo date("Y-m-d H:i:s",$stats['mTime']); ?></td>
            <td><?php echo $stats['mimeType']; ?></td>
            <td style="white-space:nowrap;font-size:smaller;text-align:right"><?php echo $stats['sizeFormatted']; ?></td>
            <td style="white-space:nowrap;font-size:smaller">
                <a href="javascript:void(0)" onclick="selectMedia('file','<?php echo $filePath; ?>','<?php echo $stats['name']; ?>');showDialog('rename');" title="<?php echo Yii::t('main', 'Rename file {file}', array('{file}'=>$stats['name'])); ?>"><?php echo CHtml::encode(Yii::t('main', 'IconRename')); ?></a>&nbsp;
                <a href="javascript:void(0)" onclick="selectMedia('file','<?php echo $filePath; ?>','<?php echo $stats['name']; ?>');showDialog('copy');" title="<?php echo Yii::t('main', 'Copy file {file}', array('{file}'=>$stats['name'])); ?>"><?php echo CHtml::encode(Yii::t('main', 'IconCopy')); ?></a>&nbsp;
                <a href="javascript:void(0)" onclick="selectMedia('file','<?php echo $filePath; ?>','<?php echo $stats['name']; ?>');showDialog('move');" title="<?php echo Yii::t('main', 'Move file {file} to another location', array('{file}'=>$stats['name'])); ?>"><?php echo CHtml::encode(Yii::t('main', 'IconMove')); ?></a>&nbsp;
                <a href="javascript:void(0)" onclick="selectMedia('file','<?php echo $filePath; ?>','<?php echo $stats['name']; ?>');showDialog('delete');" title="<?php echo Yii::t('main', 'Delete file {file}', array('{file}'=>$stats['name'])); ?>"><?php echo CHtml::encode(Yii::t('main', 'IconDelete')); ?></a>
            </td>
        </tr>
        <?php endforeach; ?>
        <!-- }}} -->
    </tbody>
</table>

<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'imagePreview',
    // additional javascript options for the dialog plugin
    'options'=>array(
        'title'=>'Selected Item',
        'autoOpen'=>false,
        'modal'=>true,
        'position'=>'center',
        'width'=>500,
    ),
)); ?>
<div id="imagePlaceholder" style="width:100%"></div>
<?php $this->endWidget(); ?>

<?php $this->renderPartial('_mediaAction',array('model'=>$mediaAction)); ?>
<?php Yii::app()->clientScript->registerScript('highlightRow',
"$('.dirsFilesRows').hover(function(){
			$(this).children().addClass('datahighlight2');
		},function(){
			$(this).children().removeClass('datahighlight2');
		});
$('.dirsFilesRows td').click(function() {
        $(this).closest('tr').siblings().removeClass('datahighlight2').removeClass('datahighlight');
        $(this).parents('tr').toggleClass('datahighlight2', this.clicked);
    });
"); ?>
<script type="text/javascript">
<!--
    var currentPath = '<?php echo $currentPath; ?>';
    var selectedItem = '.';
    var doShowDialog=false;
    // {{{ selectMedia
    function selectMedia(mediaType, path, name)
    {
        if(name=='..') 
            doShowDialog=false;
        else
            doShowDialog=true;
        jQuery('#DDMediaAction_mediaType').val(mediaType);
        jQuery('#DDMediaAction_path').val(path);
        jQuery('#DDMediaAction_name, #DDMediaAction_oldName').val(name);
        selectedItem = name;
    } // }}} 
    // {{{ showDialog
    function showDialog(action)
    {
        if(doShowDialog==false)
            return;
        jQuery('#mydialog').dialog('open');
        jQuery('#mydialog').dialog({title:'<?php echo CHtml::encode(Yii::t('main', 'Item: ')); ?>'+selectedItem /* +' &rArr; '+action */ });
        jQuery('#DDMediaAction_action').val(action);
        jQuery('#mediaActionSubmitButton').val('Submit');
        jQuery('.msg').html('').hide();
        jQuery('#selectedItemName').html(selectedItem);
        jQuery('#nameRow, #nameRowDisplayOnly, #p1Row, #uploadedFileRow').hide();
        switch(action)
        {
            case 'rename':
                jQuery('#mediaActionSubmitButton').val('<?php echo CHtml::encode(Yii::t('main', 'Rename')); ?>');
                jQuery('.msg').html('<?php echo CHtml::encode(Yii::t('main', 'Enter the new name:')); ?>').show();
                jQuery('#nameRowDisplayOnly, #p1Row').show();
                jQuery('label[for=DDMediaAction_p1]').html('<?php echo CHtml::encode(Yii::t('main', 'New Name')); ?>');
                jQuery('#DDMediaAction_p1').val(jQuery('#DDMediaAction_name').val());
                jQuery('#DDMediaAction_p1').focus().select();
                break;
            case 'copy':
                jQuery('#mediaActionSubmitButton').val('<?php echo CHtml::encode(Yii::t('main', 'Copy')); ?>');
                jQuery('.msg').html('<?php echo CHtml::encode(Yii::t('main', 'Enter the new destination and name:')); ?>').show();
                jQuery('#nameRowDisplayOnly, #p1Row').show();
                jQuery('label[for=DDMediaAction_p1]').html('<?php echo CHtml::encode(Yii::t('main', 'New Dest. and Name')); ?>');
                jQuery('#DDMediaAction_p1').val('./'+jQuery('#DDMediaAction_name').val());
                jQuery('#DDMediaAction_p1').focus().select();
                break;
            case 'move':
                jQuery('#mediaActionSubmitButton').val('<?php echo CHtml::encode(Yii::t('main', 'Move')); ?>');
                jQuery('.msg').html('<?php echo CHtml::encode(Yii::t('main', 'Enter the new location:')); ?>').show();
                jQuery('#nameRowDisplayOnly, #p1Row').show();
                jQuery('label[for=DDMediaAction_p1]').html('<?php echo CHtml::encode(Yii::t('main', 'Destination')); ?>');
                jQuery('#DDMediaAction_p1').val('./'+selectedItem);
                jQuery('#DDMediaAction_p1').focus().select();
                break;
            case 'delete':
                jQuery('#mediaActionSubmitButton').val('<?php echo CHtml::encode(Yii::t('main', 'Delete')); ?>');
                jQuery('.msg').html('<?php echo CHtml::encode(Yii::t('main', 'Confirm to delete this item:')); ?>').show();
                jQuery('#nameRowDisplayOnly').show();
                jQuery('label[for=DDMediaAction_p1]').html('<?php echo CHtml::encode(Yii::t('main', 'File to delete')); ?>');
                break;
            case 'newdir':
                jQuery('#mediaActionSubmitButton').val('<?php echo CHtml::encode(Yii::t('main', 'Create')); ?>');
                jQuery('.msg').html('<?php echo CHtml::encode(Yii::t('main', 'Enter the name for the new directory:')); ?>').show();
                jQuery('#DDMediaAction_path').val(currentPath);
                jQuery('#p1Row').show();
                jQuery('label[for=DDMediaAction_p1]').html('<?php echo CHtml::encode(Yii::t('main', 'New Directory')); ?>');
                jQuery('#DDMediaAction_p1').val('').focus().select();
                break;
            case 'upload':
                jQuery('#mediaActionSubmitButton').val('<?php echo CHtml::encode(Yii::t('main', 'Upload')); ?>');
                jQuery('.msg').html('<?php echo CHtml::encode(Yii::t('main', 'Select a file to be uploaded:')); ?>').show();
                jQuery('#DDMediaAction_path').val(currentPath);
                jQuery('#uploadedFileRow').show();
                jQuery('#DDMediaAction_uploadedFile').focus().select();
                break;
        }
        doShowDialog=false;
    } // }}} 
// -->
</script>
